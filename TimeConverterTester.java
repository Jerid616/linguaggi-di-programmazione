import java.util.Scanner;
public class TimeConverterTester {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Inserisci il numero di giorni: ");
		int d=in.nextInt();
		System.out.println("Inserisci il numero di ore: ");
		int h=in.nextInt();
		System.out.println("Inserisci il numero di minuti: ");
		int m=in.nextInt();
		System.out.println("Inserisci il numero di secondi: ");
		int s=in.nextInt();
		TimeConverter t=new TimeConverter(s, m, h, d);
		System.out.println("Convertito in secondi: "+t.getSecondiTotali(s, m, h, d));
	}
}