//Esercizio 3.1: conto in banca
public class BankAccount {
	private double balance;

	//costruttore di default
	public BankAccount() {balance=0;}
	public BankAccount(double b) {balance=b;}

	//metodi
	public void deposita(double plus) {balance+=plus;}
	public void preleva(double minus) {balance-=minus;}
	public void addInteresse(double i) {balance+=((balance+i)/100);}
	public double getSaldo() {return balance;}
}