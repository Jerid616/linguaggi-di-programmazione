public class BankMain {
	public static void main(String[] args) {
		BankAccount harrysChecking=new BankAccount();
		harrysChecking.deposita(1000);
		harrysChecking.preleva(500);
		harrysChecking.preleva(400);
		System.out.println("Harry's checking: "+harrysChecking.getSaldo());
		BankAccount momsSaving=new BankAccount(1000);
		momsSaving.addInteresse(10);
		System.out.println("Mom's saving: "+momsSaving.getSaldo());
	}
}