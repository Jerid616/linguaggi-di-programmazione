//Esercizio 5.4: Voti
public class Grade {
	private double votoNumerico;
	private String votoLettera, segno;
	public Grade() {}
	public double getVotoNumerico(String s) {
		votoLettera=s.substring(0,1); //accetta massimo 2 caratteri
		segno=s.substring(1); //di cui il segno è appunto il secondo
		if(votoLettera.equalsIgnoreCase("A")) {
			votoNumerico=4;
			if(segno.equals("-")) {votoNumerico-=0.3;}
		} else if(votoLettera.equalsIgnoreCase("B")) {
			votoNumerico=3;
		} else if(votoLettera.equalsIgnoreCase("C")) {
			votoNumerico=2;
		} else if(votoLettera.equalsIgnoreCase("D")) {
			votoNumerico=1;
		} else if(votoLettera.equalsIgnoreCase("F")) {
			votoNumerico=0;
		} else { //non sarà il massimo come gestione degli errori ma almeno non stampa i voti che non dovrebbe
			System.out.println("Lettera sbagliata!");
			return -1;
		}

		if(votoLettera.equalsIgnoreCase("B") || votoLettera.equalsIgnoreCase("C") || votoLettera.equalsIgnoreCase("D")) {
			if(segno.equals("+")) {
				votoNumerico+=0.3;
			} else if(segno.equals("-")) {
				votoNumerico-=0.3;
			}
		}

		return votoNumerico;
	}
}