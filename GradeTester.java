import java.util.Scanner;
public class GradeTester {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		Grade voto1=new Grade();
		while(true) {
			System.out.println("Scrivi il voto da A a F da convertire. Premi Q per uscire");
			String input=in.next();
			if(input.equalsIgnoreCase("Q")) {
				break;
			} else {
				System.out.println(voto1.getVotoNumerico(input));
			}		
		}
	}
}