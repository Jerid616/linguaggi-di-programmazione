import java.util.Scanner;
public class BankWithPasswordMain {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		BankWithPassword conto1=new BankWithPassword(in.nextInt());
		conto1.deposita(1000);
		conto1.preleva(500);
		conto1.preleva(400);
		System.out.println("Harry's checking: "+conto1.getSaldo());
		BankWithPassword conto2=new BankWithPassword(1000, in.nextInt());
		conto2.addInteresse(10);
		System.out.println("Mom's saving: "+conto2.getSaldo());
	}
}