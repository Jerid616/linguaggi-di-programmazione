//Esercizio 2.2: Dado
import java.util.Random; //necessaria per il random
public class Dado {
	private int nFacce;
	private Random gen; //l'oggetto seme che genera il risultato pseudocasuale da 1 a nFacce

	//costruttore che crea un dado a 6 facce
	public Dado() {
		nFacce=6;
		gen=new Random();
	}

	//costruttore che crea un dado a quante facce voglio
	public Dado(int n) {
		nFacce=n;
		gen=new Random();
	}

	//metodo che cambia il numero di facce del dado
	public void setFacce(int n) {nFacce=n;}
	
	//metodo che simula il lancio del dado
	public int lancia() {
		int risultato=gen.nextInt(nFacce)+1;
		return risultato;
	}
}