import java.util.Scanner;
public class SecondConverterTester {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Inserisci il numero di secondi: ");
		int s=in.nextInt();
		SecondConverter t=new SecondConverter(s);
		System.out.println("Convertito in: "+t.getGiorni(s)+" giorni, "+t.getOre(s)+" ore, "+t.getMinuti(s)+" minuti, "+t.getSecondi(s)+" secondi");
	}
}