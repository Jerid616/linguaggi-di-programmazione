import java.util.Scanner;
public class InputCheckerTester {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Vuoi continuare?");

		//creo una variabile che mi salvi la risposta in input nella stringa		
		String risp=in.nextLine();

		//creo un oggetto InputChecker
		InputChecker input=new InputChecker(risp);

		//stampo la risposta
		System.out.println(input.risposta()); 
	}
}

