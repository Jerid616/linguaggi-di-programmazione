//Esercizio 4.1: Power Generator
import java.lang.Math;
public class PowerGenerator {
	private double base;
	private int esponente;
	public PowerGenerator() {
		base=0.0;
		esponente=0;
	}

	public double getBase() {return base;}
	public int getEsponente() {return esponente;}
	public void setBase(double b) {base=b;}
	public void setEsponente(int e) {esponente=e;}
	public double nextPow() {
		double risultato=Math.pow(base, esponente);
		esponente++;
		return risultato;
	}
}