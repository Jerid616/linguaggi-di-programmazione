//Esercizio 5.3: anno bisestile
public class Year {
	private int anno;
	public Year(int a) {anno=a;}
	public int getAnno() {return anno;}
	public boolean isLeapYear() {
		if(anno%400==0 || (anno%4==0 && anno%100!=0)) {return true;}
		return false;
	}
}