//Esercizio 3.4 : Impiegato
public class Employee {
	private String nome;
	private double stipendio;

	//costruttore senza parametri
	public Employee() {
		nome=null;
		stipendio=0;
	}

	//costruttore con parametri
	public Employee(String n, double s) {
		nome=n;
		stipendio=s;
	}

	public String getNome() {return nome;}
	public double getStipendio() {return stipendio;}
	public void setNome(String n) {nome=n;}
	public void setStipendio(double s) {stipendio=s;}

	//3.5
	public void raiseSalary(double byPercent) {stipendio+=((stipendio*byPercent)/100);}
}