import java.util.Scanner;
public class SortTester {
	public static void main (String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Inserisci tre numeri: ");
		double a=in.nextDouble();
		double b=in.nextDouble();
		double c=in.nextDouble();
		Sort s=new Sort();
		s.ordina(a, b, c);
		System.out.println("Numeri in ordine: ");
		System.out.println(s.getA());
		System.out.println(s.getB());
		System.out.println(s.getC());
	}
}