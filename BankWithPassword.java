//Esercizio 3.2: banca con password
public class BankWithPassword {
	private double balance;
	private int password;

	//costruttore di default
	public BankWithPassword(int p) {
		balance=0;
		password=p;
	}

	public BankWithPassword(double b, int p) {
		balance=b;
		password=p;
	}

	//metodi
	public void deposita(double plus) {balance+=plus;}
	public void preleva(double minus) {balance-=minus;}
	public void addInteresse(double i) {balance+=((balance+i)/100);}
	public double getSaldo() {return balance;}
	public int getPassword() {return password;}
	public boolean controlloPassword(int p) {
		if(password==p) {return true;}
		return false;
	}

	public boolean controlloPrelievo(int s) {
		if(balance>=s) {return true;}
		return false;
	}
}