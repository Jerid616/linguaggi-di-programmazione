//Esercizio 5.1: controlla input
public class InputChecker {
	private String answer;
	public InputChecker(String a) {
		answer=a;
	}

	public String risposta() {
		if(answer.equalsIgnoreCase("S") || answer.equalsIgnoreCase("Sì") || answer.equalsIgnoreCase("Ok") || answer.equalsIgnoreCase("Certo") || answer.equalsIgnoreCase("Perché no?")) {
			return "OK";
		} else if(answer.equalsIgnoreCase("N") || answer.equalsIgnoreCase("No")) {
			return "Fine";
		} else {
			return "Dato non corretto";
		}
	}
}