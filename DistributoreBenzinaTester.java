//3.3d
import java.util.Scanner;
public class DistributoreBenzinaTester {
	public static void main(String[] args) {
		//3.3.d
		Scanner input=new Scanner(System.in);
		System.out.println("Crea un nuovo distributore di benzina: inserisci il prezzo per litro: ");
		double prezzoLitro=input.nextDouble();
		System.out.println("Crea un nuovo distributore di benzina: inserisci il tipo di carburante: gasolio o verde");
		String carburante=input.next();
		//DistributoreBenzina esso=new DistributoreBenzina(1.20); //3.3c
		DistributoreBenzina green=new DistributoreBenzina(prezzoLitro, carburante);
		System.out.println("Crea un nuovo distributore di benzina: inserisci il prezzo per litro: ");
		double prezzoLitr=input.nextDouble();
		System.out.println("Crea un nuovo distributore di benzina: inserisci il tipo di carburante: gasolio o verde");
		String carburant=input.next();
		DistributoreBenzina diesel=new DistributoreBenzina(prezzoLitr, carburant);
		System.out.println("Crea una nuova auto: inserisci i km/litro: ");
		double kmPerLitro=input.nextDouble();
		System.out.println("Crea una nuova auto: inserisci il tipo di carburante: gasolio o verde");
		String carburan=input.next();
		Car golf=new Car(kmPerLitro, carburan);
		//esso.rifornisci(1000); //3.3c
		diesel.rifornisciGasolio(1000);
		golf.addGas(20);
		diesel.vendi(20, golf);
		golf.drive(100);
		System.out.println("Livello carburante: "+golf.getGas()+" litri");
		System.out.println("Il distributore ha ancora "+diesel.getBenzina()+" litri");
		
		/*3.3c
		esso.vendi(70);
		diesel.vendi(70);
		System.out.println("Deposito attuale: "+esso.getBenzina());
		System.out.println("Deposito atteso: 941.6 periodico");
		esso.vendi(30);
		System.out.println("Deposito attuale: "+esso.getBenzina());
		System.out.println("Deposito atteso: 916.6 periodico");
		esso.aggiorna(1.30);
		esso.vendi(20);
		System.out.println("Deposito attuale: "+esso.getBenzina());
		System.out.println("Deposito atteso: 901.2821 (arrotondato)");*/		
	}
}