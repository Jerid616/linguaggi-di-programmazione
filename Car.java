//Esercizio 3.3: Macchina
public class Car {
	private double carburante;
	private double kmL;

	//esercizio 3.3b
	private String benzina;

	//costruttore con resa, e (3.3b) carburante
	public Car(double resa, String b) {
		kmL=resa;
		carburante=0;
		benzina=b;
	}

	public void drive(double km) {carburante-=(km/kmL);}
	public double getGas() {return carburante;}
	public void addGas(double plus) {carburante+=plus;}

	//esercizio 3.3b
	public boolean isDiesel() {
		if(benzina.equals("gasolio")) {return true;}
		return false;	
	}	
}