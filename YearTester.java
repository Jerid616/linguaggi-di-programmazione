import java.util.Scanner;
public class YearTester {
	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		System.out.println("Inserisci un anno: ");
		Year anno=new Year(in.nextInt());
		if(anno.isLeapYear()==true) {
			System.out.println("L'anno "+anno.getAnno()+" è bisestile");
		} else {
				System.out.println("L'anno "+anno.getAnno()+" non è bisestile");
		}
	}
}