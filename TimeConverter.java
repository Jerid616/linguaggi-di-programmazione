//Esercizio 4.2: Time Converter
public class TimeConverter {
	private int secondi, minuti, ore, giorni;
	public TimeConverter(int s, int m, int h, int d) {
		secondi=s;
		minuti=m;
		ore=h;
		giorni=d;
	}

	public int getSecondiTotali(int s, int m, int h, int d) {return (s+(60*m)+(3600*h)+(86400*d));}
}