//Esercizio 2.1: Rettangolo
public class Rettangolo {
	private int base, altezza, ascissa, ordinata;

	//costruttore coi valori predefiniti
	public Rettangolo() {
		base=2;
		altezza=1;
		ascissa=0;
		ordinata=0;
	}

	//costruttore libero
	public Rettangolo(int b, int h, int x, int y) {
		base=b;
		altezza=h;
		ascissa=x;
		ordinata=y;
	}

	//metodi getter superflui ai fini dello svolgimento ma riportati per completezza
	public int getBase() {return base;}
	public int getAltezza() {return altezza;}
	public int getX() {return ascissa;}
	public int getY() {return ordinata;}

	//metodi getter utilizzati effettivamente
	public int getPerimetro() {return 2*(base+altezza);}
	public int getArea() {return base*altezza;}

	//metodi setter, superflui ai fini dello svolgimento ma riportati per completezza
	public void setBase(int b) {base=b;}
	public void setAltezza(int h) {altezza=h;}
	public void setX(int x) {ascissa=x;}
	public void setY(int y) {ordinata=y;}

	//traslazione, superflua ai fini dello svolgimento ma riportata per completezza
	public void traslazione(int trX, int trY) {
		ascissa+=trX;
		ordinata+=trY;
	}
}