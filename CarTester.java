public class CarTester {
	public static void main(String[] args) {
		Car ibiza=new Car(20, "gasolio"); //carburante (3.3b)
		ibiza.addGas(20);
		ibiza.drive(100);
		System.out.println("Livello carburante: "+ibiza.getGas());
		System.out.println("L'auto va a gasolio: "+ibiza.isDiesel());
	}
}