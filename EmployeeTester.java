public class EmployeeTester {
	public static void main(String[] args) {
		Employee d = new Employee();
		d.setNome("Rossi");
		d.setStipendio(1000);

		//3.5
		d.raiseSalary(10);
		System.out.println(d.getNome()+" prende "+d.getStipendio()+" € al mese");
	}
}