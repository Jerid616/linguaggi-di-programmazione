//Esercizio 4.3: Second Converter
public class SecondConverter {
	private int secondi; 
	public SecondConverter(int s) {secondi=s;}

	//i 4 metodi
	public int getGiorni(int s) {return s/86400;}
	public int getOre(int s) {return (s%86400)/3600;}
	public int getMinuti(int s) {return ((s%86400)%3600)/60;}
	public int getSecondi(int s) {return (((s%86400)%3600)%60)/s;}
}