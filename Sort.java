//Esercizio 5.2: ordinamento
public class Sort {
	private double a, b, c;
	public Sort () {
		a=0.0;
		b=0.0;
		c=0.0;
	}

	public void ordina(double qui, double quo, double qua) {
		double tmp=0.0;
		a=qui;
		b=quo;
		c=qua;
		if(a<b) {
			if(c<b) {
				if(c<a) {
					tmp=a;
					a=c;
					c=b;
					b=tmp;
				} else {
					tmp=b;
					b=c;
					c=tmp;
				}
			}
		} else if(b<a) {
			if(c<a) {
				if(b<c) {
					tmp=a;
					a=b;
					b=c;
					c=tmp;
				} else {
					tmp=a;
					a=c;
					c=tmp;
				}
			} else {
				tmp=a;
				a=b;
				b=tmp;
			}
		}
	}

	public double getA() {return a;}
	public double getB() {return b;}
	public double getC() {return c;}
}