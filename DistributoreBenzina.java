//Esercizio 3.3c: Distributore di benzina
public class DistributoreBenzina {
	private double deposito;
	private double euroPerLitro;

	//3.3d
	private String carburante;
	public DistributoreBenzina(double ePL, String c) {
		deposito=0;
		euroPerLitro=ePL;
		carburante=c;
	}

	//3.3d
	public void rifornisciVerde(double unaQuantita) {deposito+=unaQuantita;}
	public void rifornisciGasolio(double unaQuantita) {deposito+=unaQuantita;}
	public void vendi(double euro, Car c) {
		if(carburante.equals("verde") || carburante.equals("gasolio")) {
			deposito-=(euro/euroPerLitro);
		} else {
			System.out.println("L'auto non va né a benzina verde né a gasolio");
		}
	}

	public void aggiorna(double unPrezzoPerLitro) {euroPerLitro=unPrezzoPerLitro;}
	public double getBenzina() {return deposito;}
}